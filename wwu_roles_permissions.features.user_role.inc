<?php
/**
 * @file
 * wwu_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function wwu_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Contributor.
  $roles['contributor'] = array(
    'name' => 'contributor',
    'weight' => 2,
  );

  // Exported role: Editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 3,
  );

  // Exported role: Super Editor.
  $roles['super Editor'] = array(
    'name' => 'super Editor',
    'weight' => 4,
  );

  return $roles;
}
